extern crate minifb;

use minifb::{Key, Window, WindowOptions};

const WIDTH: usize = 1024;
const HEIGHT: usize = 768;

fn main() {
    let image = {
        use std::io::Cursor;
        let data = &include_bytes!("pikachu.png")[..];
        image::load(Cursor::new(data), image::PNG)
            .unwrap()
            .to_rgba()
    };

    let mut window = match Window::new(
        "Noise Test - Press ESC to exit",
        WIDTH,
        HEIGHT,
        WindowOptions {
            resize: true,
            ..WindowOptions::default()
        },
    ) {
        Ok(win) => win,
        Err(err) => {
            println!("Unable to create window {}", err);
            return;
        }
    };

    // TODO: can we make the actual image be the backing buffer?
    let mut buffer: Vec<u32> = vec![0; WIDTH * HEIGHT];

    let mut size = (0, 0);

    let mut frame_count = 0;
    let start = std::time::Instant::now();

    while window.is_open() && !window.is_key_down(Key::Escape) {
        frame_count += 1;
        {
            let new_size = window.get_size();
            if new_size != size {
                size = new_size;
                buffer.resize(size.0 * size.1, 0);
            }
        }

        for (x, y, pixel) in image.enumerate_pixels() {
            let r: u32 = pixel[0].into();
            let g: u32 = pixel[1].into();
            let b: u32 = pixel[2].into();

            let index = y as usize * WIDTH + x as usize;
            if index < buffer.len() {
                buffer[index] = (r << 16) | (g << 8) | (b << 0);
            }
        }

        window.get_keys().map(|keys| {
            for t in keys {
                match t {
                    Key::W => println!("holding w!"),
                    Key::T => println!("holding t!"),
                    _ => (),
                }
            }
        });

        // We unwrap here as we want this code to exit if it fails
        window.update_with_buffer(&buffer).unwrap();
        std::thread::sleep(std::time::Duration::from_millis(30));
    }

    let dt = std::time::Instant::now().duration_since(start).as_secs();
    if dt > 0 {
        println!("FPS: {}/{} = {}", frame_count, dt, frame_count / dt);
    }
}
